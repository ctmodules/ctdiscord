package ru.will0376.ctmodules.ctdiscord;

import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Config {
	private final String DISCORD = "Discord";
	private Configuration configuration;
	private String token;
	private String ChannelID;
	private String ChannelReportID;
	private String adminGroup;
	private String prefix;
	private String botName;
	private List<String> whitelist;
	private boolean printRequestFromConsole;
	private boolean printRequestFromDaemon;
	private boolean enabled;

	public Config(File file) {
		this.configuration = new Configuration(file);
	}

	public void launch() {
		load();
		setConfigs();
		save();
	}

	private void load() {
		this.configuration.load();
	}

	private void save() {
		this.configuration.save();
	}

	private void setConfigs() {
		enabled = configuration.getBoolean("enabled", DISCORD, true, "http://tetraquark.ru/archives/258");

		token = configuration.getString("token", DISCORD, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX(59)", "");
		ChannelID = configuration.getString("ChannelID", DISCORD, "XXXXXXXXXXXXXXXXXX(18)", "");
		ChannelReportID = configuration.getString("ChannelReportID", DISCORD, "XXXXXXXXXXXXXXXXXX(18)", "(if module 'ctreport' used)");
		adminGroup = configuration.getString("adminGroup", DISCORD, "Moderator", "");
		prefix = configuration.getString("prefix", DISCORD, "!", "Prefix for commands.");
		botName = configuration.getString("botName", DISCORD, "Botname", "");

		whitelist = Arrays.asList(configuration.getStringList("whitelist", DISCORD, new String[]{"will0376"}, ""));

		printRequestFromConsole = configuration.getBoolean("printRequestFromConsole", DISCORD, true, "Print all requests from the console");
		printRequestFromDaemon = configuration.getBoolean("printRequestFromDaemon", DISCORD, false, "Print all requests from the daemon");
	}

	public String getToken() {
		return token;
	}

	public String getChannelID() {
		return ChannelID;
	}

	public String getAdminGroup() {
		return adminGroup;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getBotName() {
		return botName;
	}

	public List<String> getWhitelist() {
		return whitelist;
	}

	public String getChannelReportID() {
		return ChannelReportID;
	}

	public boolean isPrintRequestFromConsole() {
		return printRequestFromConsole;
	}

	public boolean isPrintRequestFromDaemon() {
		return printRequestFromDaemon;
	}

	public boolean isEnabled() {
		return enabled;
	}
}
