package ru.will0376.ctmodules.ctdiscord;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.ctmodules.events.EventCheckTokenFailed;
import ru.will0376.ctmodules.events.EventPrintAnswer;
import ru.will0376.ctmodules.events.EventPrintHelp;
import ru.will0376.ctmodules.events.EventReloadConfig;
import ru.will0376.ctmodules.events.utils.Logger;
import ru.will0376.ctmodules.events.utils.Utils;

@Mod.EventBusSubscriber
public class Events {
	@SubscribeEvent
	public static void printToDiscordEvent(EventPrintAnswer e) {
		if (Ctdiscord.config.isEnabled()) {
			if (e.isReport()) {
				String tmp = String.format("Жалоба от: %s,\nНа игрока: %s,\nСсылка: %s", e.getAdminNick(), e.getPlayerNick(), e.getText());
				Discord.printToDiscordChannel(tmp);
			} else if (!e.getAdminNick().equals("Server")
					|| (e.getAdminNick().equals("Server") && Ctdiscord.config.isPrintRequestFromConsole())
					|| (e.getAdminNick().equals("daemon") && Ctdiscord.config.isPrintRequestFromDaemon())) {
				String tmp = String.format("Screen from %s\nby admin %s\nwith link %s", e.getPlayerNick(), e.getAdminNick(), e.getText());
				Discord.printToDis(tmp);
			}
		}
	}

	@SubscribeEvent
	public static void catchCheckingToken(EventCheckTokenFailed e) {
		if (Ctdiscord.config.isEnabled()) {
			if (e.getAdminNick().equals(Ctdiscord.config.getBotName()))
				Discord.printToDis(String.format("[Discord_token] Error! Player %s has invalid token: %s", e.getPlayerNick(), e.getToken()));
		}
	}

	@SubscribeEvent
	public static void printHelp(EventPrintHelp e) {
		e.printToPlayer(Utils.makeUsage(Ctdiscord.MOD_ID, Commands.usage, Commands.aliases, Commands.permissions));
	}

	@SubscribeEvent
	public static void reload(EventReloadConfig e) {
		Ctdiscord.config.launch();
		Discord.restartBot();
		if (Utils.getEPMP(e.getSender()) == null) Logger.log(1, "[Ctdiscord]", "Reloaded");
		else
			Utils.getEPMP(e.getSender()).sendMessage(new TextComponentString(TextFormatting.GOLD + "[Ctdiscord] Reloaded"));
	}
}
