package ru.will0376.ctmodules.ctdiscord;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.message.MessageCreateEvent;
import ru.will0376.ctmodules.events.EventRequestCrash;
import ru.will0376.ctmodules.events.EventRequestScreen;
import ru.will0376.ctmodules.events.utils.Logger;

import java.awt.*;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class Discord {
	public static DiscordApi api;
	public static Thread disBot;
	private static final HashMap<String, Long> cooldown = new HashMap<>();

	public static void DiscordThread() {
		disBot = new Thread(new DiscordBot());
		disBot.start();
	}

	@SuppressWarnings("deprecation")
	public static void killbot() {
		if (api != null)
			api.disconnect();

		if (disBot.isAlive())
			disBot.stop();
	}

	public static void printToDis(String text) {
		long time = System.currentTimeMillis() / 1000;
		if (api != null) {
			if (cooldown.containsKey(text)) {
				if (!(cooldown.get(text) > time)) {
					cooldown.remove(text);
					cooldown.put(text, time + 2);
					getChannel().sendMessage(text);
					Logger.log(3, "[DiscordBot]", "Message: " + text);
				}
			} else {
				cooldown.put(text, time + 2);
				getChannel().sendMessage(text);
				Logger.log(3, "[DiscordBot]", "Message: " + text);
			}


		} else {
			Logger.log(3, "[DiscordBot]", "api == null =\\");
		}
	}

	public static void printToDiscordChannel(String text) {
		if (api != null) {
			getReportChannel().sendMessage(text);
			Logger.log(3, "[DiscordBot]", "Message: " + text);
		} else {
			Logger.log(3, "[DiscordBot]", "api == null =\\");
		}
	}

	public static void restartBot() {
		killbot();
		DiscordThread();
	}

	static TextChannel getChannel() {
		return api.getChannelById(Ctdiscord.config.getChannelID()).get().asTextChannel().get();
	}

	static TextChannel getReportChannel() {
		return api.getChannelById(Ctdiscord.config.getChannelReportID()).get().asTextChannel().get();
	}
}

class DiscordBot implements Runnable {
	@Override
	public void run() {
		if (Ctdiscord.config.isEnabled()) logic();
	}

	public void logic() {
		try {
			String token = Ctdiscord.config.getToken();
			String ChannelID = Ctdiscord.config.getChannelID();

			if (token.equals("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX(59)"))
				Logger.log(1, "[Discord-ERROR]", "Discord token incorrect");
			if (ChannelID.equals("XXXXXXXXXXXXXXXXXX(18)"))
				Logger.log(1, "[Discord-ERROR]", "Discord ChannelID incorrect");

			Discord.api = new DiscordApiBuilder().setToken(token).login().join();
			Logger.log(3, "[Discord]", "Created Instance of api Builder");
			Logger.log(3, "[Discord]", "Await Ready");
			Discord.api.addMessageCreateListener(event -> {
				if (event.getChannel().getId() == Long.parseLong(ChannelID)) {
					Message message = event.getMessage();

					Logger.log(3, "[From Discord]", message.getAuthor().getName() + ":" + message.getContent());
					String[] msg = message.getContent().split(" ");

					if (message.getContent().contains(Ctdiscord.config.getPrefix()))
						if (checkPerm(event)) {
							switch (msg[0].replace(Ctdiscord.config.getPrefix(), "").toLowerCase()) {
								case "check":
									adminCheck(msg);
									break;

								case "checkall":
									adminCheckAll(msg);
									break;

								case "crashall":
									adminCrashAll(msg);
									break;

								case "help":
									printHelpForAdmin();
									break;

								case "crash":
									adminCrash(msg);
									break;

								case "list":
									admingetserverlist();
									break;
								default:
									Discord.printToDis("Unknown");
							}
						} else {
							Discord.printToDis("No rights!");
						}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void admingetserverlist() {
		StringBuilder tmp = new StringBuilder();
		tmp.append(String.format("[%s/%s] Players online:\n", FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers().size(), FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getMaxPlayers()));
		for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers())
			tmp.append(player.getName()).append("\n");
		Discord.printToDis(tmp.toString());
	}


	private void adminCrashAll(String[] msg) {
		for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers())
			if (player != null)
				if (Ctdiscord.INSTANCE.checkOnWhitelist(player.getName()) || contains(msg, "-f"))
					MinecraftForge.EVENT_BUS.post(new EventRequestCrash(Ctdiscord.config.getBotName(), player.getName()));
	}

	private void adminCrash(String[] msg) {
		String Usage = Ctdiscord.config.getPrefix() + "crash <player>";
		if (msg.length == 1) {
			Discord.printToDis("___Usage: " + Usage + "___");
			return;
		}
		if (!whitelistCheck(msg)) {
			Discord.printToDis("No rights!");
			return;
		}
		if (getEPMP(msg[msg.length - 1]) != null)
			MinecraftForge.EVENT_BUS.post(new EventRequestCrash(Ctdiscord.config.getBotName(), msg[msg.length - 1]));
	}

	private void adminCheckAll(String[] msg) {
		for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
			if (player != null)
				if (Ctdiscord.INSTANCE.checkOnWhitelist(player.getName()) || contains(msg, "-f")) {
					MinecraftForge.EVENT_BUS.post(new EventRequestScreen(Ctdiscord.config.getBotName(), player.getName()));
				}
		}
	}

	private boolean contains(String[] msq, String check) {
		for (String tmp : msq)
			if (tmp.equals(check))
				return true;
		return false;
	}

	private void adminCheck(String[] msg) {
		String Usage = Ctdiscord.config.getPrefix() + "check <player>";
		if (msg.length == 1) {
			Discord.printToDis("___Usage: " + Usage + "___");
			return;
		}
		if (!whitelistCheck(msg)) {
			Discord.printToDis("No rights!");
			return;
		}
		if (getEPMP(msg[msg.length - 1]) != null)
			MinecraftForge.EVENT_BUS.post(new EventRequestScreen(Ctdiscord.config.getBotName(), msg[msg.length - 1]));

	}

	private boolean whitelistCheck(String[] msg) {
		for (String s : msg) {
			if (s.contains("-f") || s.contains("-force"))
				return true;
		}
		for (Object tmp : Ctdiscord.config.getWhitelist()) {
			if (tmp.toString().toLowerCase().equals(msg[msg.length - 1].toLowerCase()))
				return false;
		}
		return true;
	}

	private void printHelpForAdmin() {
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(Ctdiscord.MOD_NAME + " help");
		eb.setAuthor(Ctdiscord.config.getBotName(), null, Discord.api.getYourself().getAvatar().getUrl().toString());
		eb.setColor(Color.red);
		eb.setColor(new Color(0xF40C0C));
		eb.setColor(new Color(255, 0, 54));

		eb.setDescription("Necessary role: " + Ctdiscord.config.getAdminGroup());
		eb.addField("Screen", "``" + Ctdiscord.config.getPrefix() + "check <nick>`` - get screen from player", false);
		eb.addField("ScreenAll", "``" + Ctdiscord.config.getPrefix() + "checkall`` - get screen from all players", false);

		eb.addField("Crash", "``" + Ctdiscord.config.getPrefix() + "crash <nick>`` - crash player", false);
		eb.addField("Crash", "``" + Ctdiscord.config.getPrefix() + "crashall`` - crash all players", false);

		eb.addField("Get ServerList", "``" + Ctdiscord.config.getPrefix() + "list`` - Returns a list of server players", false);

		Discord.getChannel().sendMessage(eb);
	}

	private boolean checkPerm(MessageCreateEvent event) {
		AtomicBoolean ret = new AtomicBoolean(false);
		Logger.log(3, "[DiscordBot]", "checking perm for " + event.getMessage().getAuthor().getName());
		event.getServer().get().getMemberById(event.getMessageAuthor().getId()).get().getRoles(event.getServer().get()).forEach(role -> {
			if (role.getName().toLowerCase().equals(Ctdiscord.config.getAdminGroup().toLowerCase()))
				ret.set(true);
		});
		Logger.log(3, "[DiscordBot]", String.format("Player %s have the right: %s", event.getMessage().getAuthor().getName(), ret.get()));
		return ret.get();
	}


	private EntityPlayerMP getEPMP(String nick) {
		for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers())
			if (player.getName().toLowerCase().equals(nick.toLowerCase()))
				return player;
		Discord.printToDis("Player " + nick + " not found.");
		return null;
	}

}
