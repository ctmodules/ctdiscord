package ru.will0376.ctmodules.ctdiscord;

import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import ru.will0376.ctmodules.events.EventLogPrint;
import ru.will0376.ctmodules.events.EventModStatusResponse;
import ru.will0376.ctmodules.events.utils.Logger;

import java.util.List;

@Mod(
		modid = Ctdiscord.MOD_ID,
		name = Ctdiscord.MOD_NAME,
		version = Ctdiscord.VERSION,
		serverSideOnly = true,
		acceptableRemoteVersions = "*",
		dependencies = "required-after:events@[1.0,)"

)
public class Ctdiscord {

	public static final String MOD_ID = "ctdiscord";
	public static final String MOD_NAME = "Ctdiscord";
	public static final String VERSION = "1.0";
	public static Config config;

	@Mod.Instance(MOD_ID)
	public static Ctdiscord INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		config = new Config(event.getSuggestedConfigurationFile());
		config.launch();
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, TextFormatting.GOLD + "[Ctdiscord]" + TextFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, config.isEnabled()));
	}

	@Mod.EventHandler
	public void startingServer(FMLServerStartingEvent e) {
		Discord.DiscordThread();
		e.registerServerCommand(new Commands());
	}

	public boolean checkOnWhitelist(String nick) {
		Logger.log(3, "[Ctdiscord.main]", "Checking on Whitelist nick: " + nick);
		List<String> whitelist = config.getWhitelist();
		for (String s : whitelist)
			if (s.equals(nick))
				return false;
		return true;
	}

}
