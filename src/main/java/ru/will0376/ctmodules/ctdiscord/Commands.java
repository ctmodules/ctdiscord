package ru.will0376.ctmodules.ctdiscord;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import ru.will0376.ctmodules.events.utils.ChatForm;
import ru.will0376.ctmodules.events.utils.Utils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Commands extends CommandBase {
	public static String usage =
			" /ctmd printToDis\n" +
					" /ctmd restartDiscordBot\n" +
					" /ctmd stopDiscordBot\n" +
					" /ctmd permissions";
	public static String aliases =
			" Aliases: [ctmdis, ctm-dis, ctm-d, ctmd, ctmdiscord]";
	public static String permissions =
			" Permissions: [\n" +
					"   ctmodules.report.admin.printToDis\n" +
					"   ctmodules.report.admin.restartdiscrodbot\n" +
					"   ctmodules.report.admin.stopDiscordBot\n" +
					" ]";

	@Override
	public String getName() {
		return "ctm-discord";
	}

	@Override
	public List<String> getAliases() {
		ArrayList<String> al = new ArrayList<>();
		al.add("ctmdis");
		al.add("ctm-dis");
		al.add("ctm-d");
		al.add("ctmd");
		al.add("ctmdiscord");
		return super.getAliases();
	}

	@Override
	public List getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		if (args.length == 1)
			return getListOfStringsMatchingLastWord(args, "printToDis", "restartDiscordBot", "stopDiscordBot", "permissions");
		else
			return args.length >= 2 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return usage;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args[0].equalsIgnoreCase("printToDis")) {
			if (!Utils.checkPermission(sender, "ctmodules.admin.printToDis")) {
				return;
			}
			StringBuilder tmp = new StringBuilder();
			for (String tt : args)
				tmp.append(" ").append(tt);
			tmp = new StringBuilder(tmp.toString().replace(args[0], ""));
			Discord.printToDis(tmp.toString());
		} else if (args[0].equalsIgnoreCase("restartDiscrodBot")) {
			if (!Utils.checkPermission(sender, "ctmodules.discord.admin.restartdiscrodbot")) {
				return;
			}
			Discord.killbot();
			Discord.DiscordThread();
			sender.sendMessage(new TextComponentString(ChatForm.prefix + "Done!"));

		} else if (args[0].equalsIgnoreCase("stopDiscordBot")) {
			if (!Utils.checkPermission(sender, "ctmodules.discord.admin.stopDiscordBot")) {
				return;
			}
			Discord.killbot();
			sender.sendMessage(new TextComponentString(ChatForm.prefix + "Done!"));
		} else if (args[0].equalsIgnoreCase("permissions")) {
			sender.sendMessage(new TextComponentString(ChatForm.prefix + "Permissions:\n" + permissions));
		} else {
			sender.sendMessage(new TextComponentString(ChatForm.prefix + usage));
		}

	}
}
