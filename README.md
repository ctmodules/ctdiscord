# Основное:
**CTDiscord** - серверный модуль для приватного мода ClientTweaker (WindCheck) интегрирующий его в дискорд.

# Зависимости:
* Events https://gitlab.com/ctmodules/events
* ClientTweaker[2.0.0,)
* Включённый режим закачки на imgur в главном моде.
* Библиотека javacord-3.0.3

# Установка:
* Скачать мод с репозитория и скачать библиотеку javacord-3.0.3-shaded https://gitlab.com/ctmodules/ctdiscord/-/tree/master/libs.
* Закинуть мод в папку *mods*
* Создать в корне сервера папку willslib (для исключения путаницы) и положить библиотеку в неё.
* Отредактировать строку запуска, приведя её к виду:
`java -cp "ядро:willsLib/*" главный класс`.\
Где: \
*Ядро* - ваш jar, который запускается;\
*Главный* класс - Класс, который первым вызывается из ядра(найти можно по пути ядро.jar/META-INF/MANIFEST.MF, строка Main-Class: )

# Примеры строк запуска:
* Mohist: `java -Xms1024m -Xmx1024m -cp "Mohist-aea8958-server.jar:willsLib/*" red.mohist.Mohist`

* Forge: `java -Xms1024m -Xmx1024m -cp "forge-1.12.2-14.23.5.2834-universal.jar:willsLib/*" net.minecraftforge.fml.relauncher.ServerLaunchWrapper`

* Гравит: `java -server -Xmx1024m -Xmn1024m -cp ServerWrapper.jar:forge-1.12.2-14.23.5.2847-universal.jar:willsLib/* pro.gravit.launcher.server.ServerWrapper`

# Настройка:
Конфиг мода будет находиться по пути config/ctdiscord.cfg \
Параметры:
 * enabled - Включён ли мод в данный момент.
 
 * token - Токен бота(как получить - в гугл).
 
 * ChannelID - ID главного канала, куда будут скидываться скриншоты, заказанные администрацией.
 
 * ChannelReportID - ID второго канала, куда будут скидываться скриншоты с модуля CTReport(можно использовать такой-же как и в ChannelID) .
 
 * adminGroup - Группа, которая будет иметь доступ к командам бота (можно использовать русские символы и пробелы).
 
 * prefix - Префикс использующийся ботом для определения команд(e.x !help).
 
 * botName - Имя бота, используется для перехвата скриншота (можно использовать русские символы и пробелы).
 
 * whitelist - Белый лист людей, на которых нельзя будет заказать скриншот.
 
 * printRequestFromConsole - Перехватывать ли скриншоты, запрошенные с консоли?
 
 * printRequestFromDaemon - Перехватывать ли скриншоты, запрошенные демоном?
 
# Клонирование репозитория:
 Выполнить сразу после клонирования проекта: git submodule update --init \
 
 Для обновления модуля Event: git submodule update --remote